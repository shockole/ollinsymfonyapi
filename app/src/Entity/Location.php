<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\LocationRepository")
 */
class Location
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $zip;

    /**
     * @ORM\Column(type="integer")
     */
    private $creatorid;

    /**
     * @ORM\Column(type="integer")
     */
    private $stateid;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $creationdate;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getZip(): ?string
    {
        return $this->zip;
    }

    public function setZip(?string $zip): self
    {
        $this->zip = $zip;

        return $this;
    }

    public function getCreatorid(): ?int
    {
        return $this->creatorid;
    }

    public function setCreatorid(int $creatorid): self
    {
        $this->creatorid = $creatorid;

        return $this;
    }

    public function getStateid(): ?int
    {
        return $this->stateid;
    }

    public function setStateid(int $stateid): self
    {
        $this->stateid = $stateid;

        return $this;
    }

    public function getCreationdate(): ?string
    {
        return $this->creationdate;
    }

    public function setCreationdate(string $creationdate): self
    {
        $this->creationdate = $creationdate;

        return $this;
    }
}

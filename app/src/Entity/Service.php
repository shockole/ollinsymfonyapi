<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * * @ORM\Table(name="`service`")
 * @ORM\Entity(repositoryClass="App\Repository\ServiceRepository")
 */
class Service
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=80)
     */
    private $description;

    /**
     * @ORM\Column(type="integer", length=10)
     */
    private $price;

    /**
     * @ORM\Column(type="integer", length=100)
     */
    private $creatorid;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getPrice(): ?int
    {
        return $this->price;
    }

    public function setPrice(int $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function setCreatorId(string $creatorid): self
    {
        $this->creatorid = $creatorid;

        return $this;
    }

    public function getCreatorId(): ?int
    {
        return $this->creatorid;
    }
}

<?php

namespace App\Repository;

use App\Entity\SomeTest;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method SomeTest|null find($id, $lockMode = null, $lockVersion = null)
 * @method SomeTest|null findOneBy(array $criteria, array $orderBy = null)
 * @method SomeTest[]    findAll()
 * @method SomeTest[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SomeTestRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, SomeTest::class);
    }

    // /**
    //  * @return SomeTest[] Returns an array of SomeTest objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?SomeTest
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}

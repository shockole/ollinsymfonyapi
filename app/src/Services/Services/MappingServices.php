<?php
namespace App\Services\Services;

use App\Entity\Service;

class MappingServices
{
    // Method to map a service as an object
    public function mapServices(Service $service)
    {
        return [
            'id' => (int) $service->getId(),
            'name' => (string) $service->getName(),
            'description' => (string) $service->getDescription(),
            'price' => (int) $service->getPrice(),
            'creator' => (int) $service->getCreatorId()
        ];
    }
}

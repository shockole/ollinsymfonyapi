<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use App\Entity\User;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Doctrine\DBAL\Driver\Connection;

class AuthController extends AbstractController
{
    /**
     * @Route("/register", methods={"POST"})
     */
    public function index(Request $request, UserPasswordEncoderInterface $encoder)
    {   
        $response = new JsonResponse();

        $em = $this->getDoctrine()->getManager();
        
        $username = $request->get('username');
        $password = $request->get('password');
        
        $user = new User($username);
        $user->setPassword($encoder->encodePassword($user, $password));
        $em->persist($user);
        $em->flush();
        
        return $response->setStatusCode(201)->setData(array('message' => ('User '.$user->getUsername().' successfully created')));

        // $em = $this->getDoctrine()->getManager();
        
        // $username = $request->get('username');
        // $password = $request->get('password');
        
        // $user = new User($username);
        // $pass = $encoder->encodePassword($user, $password);
        // // $em->persist($user);
        // $em->flush('INSERT INTO user (id, username, "password", is_active, "role") VALUES (null,'.$username.','.$pass.' , 1, "user")');
        // return new Response(sprintf('User %s successfully created', $username));
    }

    /**
     * @Route("/user")
     */
    public function user(Connection $connection, Request $request)
    {
        return new Response(sprintf('Logged in as %s', $this->getUser()->getUsername()));
        // $username = $request->request->get('password');
        // $user = $connection->fetchAll('SELECT username FROM user WHERE username="ole"');
        // echo($username);
        // return new Response(sprintf('Logged in as %s', $user[0]['username']));
    }

    /**
     * @Route("/login", methods={"POST"})
     */
    public function loginCheck()
    {
    }
}

<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use App\Services\Locations\MappingLocations;
use App\Entity\Location;

class LocationController extends AbstractController
{
    /**
     * @Route("/location", methods={"POST"})
     */
    public function index(Request $request)
    {
        $response = new JsonResponse();
        
        $em = $this->getDoctrine()->getManager();
        
        $locationName = $request->get('name');
        $locationZip = $request->get('zip');
        $locationCreator = $this->getUser()->getId();
        $locationState = $request->get('stateId');
        $locationDate = gmdate("Y-m-d\TH:i:s\Z");

        $location = new Location();
        $location->setName($locationName);
        $location->setZip($locationZip);
        $location->setCreatorId($locationCreator);
        $location->setStateid($locationState);
        $location->setCreationdate($locationDate);
        
        $em->persist($location);
        $em->flush();
        return $response->setStatusCode(201)->setData(array('message' => ('Location '.$location->getName().' successfully created by '.$this->getUser()->getUsername())));
    }

    /**
     * @Route("location/", methods={"GET"})
     */
    public function getLocations()
    {
        $locationsArray = [];
        $response = new JsonResponse();
        $mappingService = new MappingLocations();
        $locations = $this->getDoctrine()->getRepository('App:Location')->findAll();
        foreach ($locations as $location) {
            $locationsArray[] = $mappingService->mapLocations($location);
        }
    
        $response->setData(array('locations' => $locationsArray));
        return $response;
    }

    /**
     * @Route("location/{id}", methods={"GET"})
     */
    public function getLocationsByID($id)
    {
        $response = new JsonResponse();
        $mappingService = new MappingLocations();
        $location = $this->getDoctrine()->getRepository('App:Location')->find($id);
        if ($location) {
            $response->setData(array('location' => $mappingService->mapLocations($location)));
            return $response;
        }
        
        return $response->setStatusCode(404)->setData(array('message' => 'Location not found'));
    }

    /**
     * @Route("location/{id}", methods={"PUT"})
     */
    public function updateLocationsByID(Request $request, $id)
    {
        $response = new JsonResponse();
        $em = $this->getDoctrine()->getManager();
        $location = $em->getRepository('App:Location')->findOneById($id);
        if ($location) {
            $locationName = $request->get('name');
            $locationZip = $request->get('zip');
            $locationState = $request->get('stateId');

            $location->setName($locationName);
            $location->setZip($locationZip);
            $location->setStateid($locationState);
            
            $em->persist($location);
            $em->flush();
            
            $response->setData(array('message' => sprintf('State %s successfully updated', $location->getName())));
            return $response;
        }
        
        return $response->setStatusCode(404)->setData(array('message' => 'State not found'));
    }

    /**
     * @Route("location/{id}", methods={"DELETE"})
     */
    public function deleteLocationsByID(Request $request, $id)
    {
        $response = new JsonResponse();
        $em = $this->getDoctrine()->getManager();
        $location = $em->getRepository('App:Location')->findOneById($id);
        if ($location) {
            $em->remove($location); 
            $em->flush();
            $response->setData(array('message' => sprintf('Location %s successfully deleted', $location->getName())));
            return $response;
        }
        
        return $response->setStatusCode(404)->setData(array('message' => 'Location not found'));
    }
}

<?php

  class DB {
    private $connection = NULL;

    function __construct() {
    }

    /**
     * method to create a new database connection
     */
    public function init() {
      if (!$connection) {
        try {
        $dsn = 'mysql:host='.DB_HOST.';dbname='.DB_NAME;
        $connection = new PDO($dsn, DB_USER, DB_PASS, array(PDO::ATTR_PERSISTENT => true));
        $connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch (PDOException $e) {
        die('Connection error: ' . $e->getMessage());
        }
      }
      return $connection;
    }

    function __destruct(){
      if ($this->connection !== NULL) {
        $this->connection = NULL;
      }
    }
  }

?> 
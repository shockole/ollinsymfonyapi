<?php

  include './.src/interfaces/controller.php';
  include './.src/utils/response.php';

  class Controller implements mainController {
    public $queryParams = NULL;
    public $body = NULL;
    public $headers = NULL;

    /**
     * default method used for the GET requests
     * @param param parameter sent on the url
     */
    public function get($param = NULL) {
      $responseData = array();
      $responseData['method'] = 'GET';
      $responseData['headers'] = json_decode($this->headers);
      $responseData['query'] = json_decode($this->queryParams);
      successResponse(200, $responseData, 'Controller working correctly');
    }

    /**
     * default method used for the POST requests
     * @param param parameter sent on the url
     */
    public function post($param = NULL) {
      $responseData = array();
      $responseData['method'] = 'POST';
      $responseData['body'] = json_decode($this->body);
      $responseData['headers'] = json_decode($this->headers);
      $responseData['query'] = json_decode($this->queryParams);
      successResponse(200, $responseData, 'Controller working correctly');
    }

    /**
     * default method used for the PUT requests
     * @param param parameter sent on the url
     */
    public function put($param = NULL) {
      $responseData = array();
      $responseData['method'] = 'PUT';
      $responseData['body'] = json_decode($this->body);
      $responseData['headers'] = json_decode($this->headers);
      $responseData['query'] = json_decode($this->queryParams);
      successResponse(200, $responseData, 'Controller working correctly');
    }

    /**
     * default method used for the DELETE requests
     * @param param parameter sent on the url
     */
    public function delete($param = NULL) {
      $responseData = array();
      $responseData['method'] = 'DELETE';
      $responseData['body'] = json_decode($this->body);
      $responseData['headers'] = json_decode($this->headers);
      $responseData['query'] = json_decode($this->queryParams);
      successResponse(200, $responseData, 'Controller working correctly');
    }

  }

?>
<?php 

  include './.src/interfaces/model.php';
  include './.src/database/db.php';

  class Model extends DB implements mainModel {
    public $db;
    public $connection;
    public $tableName;

    /**
     * class constructor to generate a new database connection and get the table name to use
     */
    public function __construct($tableName = NULL) {
      $this->db = new DB();
      $this->connection = $this->db->init();
      $this->setTableName($tableName);
    }
    
    /**
     * method to set the table name to use
     */
    public function setTableName($tableName = NULL) {
      $this->tableName = is_null($tableName) ? strtolower(get_class($this)) : $tableName;
    }

    /**
     * default method for all Models to get all data from the table by a specific id
     * @param id identifier to get all data from the table
     */
    public function getAllDataById($id) {
      $result = $this->connection->prepare('SELECT * FROM '.$this->tableName.' WHERE id = :id');
      $result->bindValue(':id', $id);
      $result->execute();
      return $result->fetchAll(PDO::FETCH_ASSOC);
    }
  }

?>
<?php

  /**
   * interface declaration for the Controllers
   */
  interface mainController {
    public function get($param);
    public function post($param);
    public function put($param);
    public function delete($param);
  }

?>
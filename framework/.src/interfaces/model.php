<?php 

  /**
   * interface declaration for the Models
   */
  interface mainModel {
    public function __construct($tableName);
    public function setTableName($tableName);
    public function getAllDataById($id);
  }

?>
<?php
  // load files required to check the allowed routes and database config 
  include(__DIR__ . '/router.php');
  require_once __DIR__ .'/config/database.php';
  $json_content = file_get_contents('./config/routes.json');
  $php_array = json_decode($json_content, true);

  // parse the headers, body and query params
  parse_str($_SERVER['QUERY_STRING'], $query_params);
  $request_body = json_decode(file_get_contents('php://input'), true);
  $request_headers = apache_request_headers();

  // Create e new Router instance to execute the correct Controller
  $Router = new Router($php_array, $query_params, $request_body, $request_headers);
  $Router->loadRoute();

?> 
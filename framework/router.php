<?php

  class Router {
    private $routes_array;
    private $query_params;
    private $request_body;
    private $request_headers;
    private $parameter;

    /**
     * default method used for the GET requests
     * @param routes_array routes allowed
     * @param query_params parameters sent in the url
     * @param request_body request body
     * @param request_headers request headers
     */
    public function __construct($routes_array = NULL, $query_params = NULL, $request_body = NULL, $request_headers = NULL) {
      $this->routes_array = is_null($routes_array) ? [] : $routes_array;
      $this->query_params = is_null($query_params) ? [] : $query_params;
      $this->request_body = is_null($request_body) ? [] : json_encode($request_body);
      $this->request_headers = is_null($request_headers) ? [] : json_encode($request_headers);
    }

    /**
     * logic to find the correct controller according the declared routes
     */
    public function loadRoute() {
      foreach($this->routes_array as $route) {
        $route_controller = preg_replace('/.php/', '', $route['controller']);
        if ($this->query_params['controller'] === $route_controller) {
          $url = $route['path'];
          if (strpos($route['path'], ':') !== false) {
            preg_match('/(\/:.+\/)|(:.+\/)|(:.+$)/', $url, $declared_param_matches);
            $param = preg_replace('/\//', '', $declared_param_matches[0]);
            $url = preg_replace('/\//', '\/', $url);
            $url = preg_replace('/'.$param.'/', '(.+)', $url);
          }
          $url = preg_replace('/^\//', '', $url);
          $url = preg_replace('/\/$/', '', $url);
          preg_match('/^\/'.$url.'$/', $_SERVER['REQUEST_URI'], $param_values_matches);
          if ($param_values_matches[0]) {
            if ($param_values_matches[1]) {
              $this->parameter = $param_values_matches[1];
            }
            $controller_available = $route;
            break;
          }
        }
      }

      if ($controller_available) {
        $controller_to_use = preg_replace('/.php/', '', $controller_available['controller']);
        $this->createClassInstance($controller_to_use.'.php');
      } else {
        echo 'The resource you requested was not found.';
        echo '<br>';
        echo 'Please check your defined routes and your controllers created.';
      }
    }

    /**
     * logic to generate a new controller instance and execute the correct logic according the http method received
     * @param controller_name controller name to execute
     */
    private function createClassInstance($controller_name) {
      $path = __DIR__ . '/controllers/'. $controller_name;
      include($path);
      $classes = get_declared_classes();
      $Class = end($classes);
      $Controller = new $Class;
      unset($this->query_params['controller']);
      $this->query_params = json_encode($this->query_params);
      switch ($_SERVER['REQUEST_METHOD']) {
        case 'GET':
          $Controller->queryParams = $this->query_params;
          $Controller->body = $this->request_body;
          $Controller->get($this->parameter);
          break;
        case 'POST':
          $Controller->queryParams = $this->query_params;
          $Controller->body = $this->request_body;
          $Controller->headers = $this->request_headers;
          $Controller->post($this->parameter);
          break;
        case 'PUT':
          $Controller->queryParams = $this->query_params;
          $Controller->body = $this->request_body;
          $Controller->headers = $this->request_headers;
          $Controller->put($this->parameter);
          break;
        case 'DELETE':
          $Controller->queryParams = $this->query_params;
          $Controller->body = $this->request_body;
          $Controller->headers = $this->request_headers;
          $Controller->delete($this->parameter);
          break;
        
        default:
          $Controller->queryParams = $this->query_params;
          $Controller->body = $this->request_body;
          $Controller->headers = $this->request_headers;
          $Controller->get($this->parameter);
          break;
      }
    }
  }

?> 
<?php

  include('./.src/controller/controller.php');
  include('./models/user.php');

  class UserController extends Controller {

    /**
     * custom method used for the GET requests
     * @param id identifier received on the url
     */
    public function get($id) {
      $model = new User();
      $result = $model->getAllDataById($id);
      if (count($result) > 0) {
        return successResponse(200, $result, 'Table data gotten successfully');
      }
      return errorResponse(404, 'User not found');
    }
  }

?>